resource "aws_api_gateway_rest_api" "MyDemoAPI" {
  name        = "MyDemoAPI"
  description = "This is my API for demonstration purposes"
}


## Client-Authorizers


resource "aws_api_gateway_authorizer" "Client-Authorizer" {
  name          = "Client-Authorizer"
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  type          = "COGNITO_USER_POOLS"
  provider_arns = [var.Client-Authorizer]
}


## Client-Resource


resource "aws_api_gateway_resource" "Client" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_rest_api.MyDemoAPI.root_resource_id
  path_part   = "Client"
}


## getBrandProducts


resource "aws_api_gateway_resource" "Client-getBrandProducts" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getBrandProducts"
}


resource "aws_api_gateway_method" "Client-getBrandProducts-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getBrandProducts.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getBrandProducts-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getBrandProducts.id
  http_method = aws_api_gateway_method.Client-getBrandProducts-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getBrandProducts-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getBrandProducts.id
  http_method = aws_api_gateway_method.Client-getBrandProducts-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getBrandProducts-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getBrandProducts.id
  http_method = aws_api_gateway_method.Client-getBrandProducts-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getBrandProducts-200.status_code
}


## getCartInstall


resource "aws_api_gateway_resource" "Client-getCartInstall" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getCartInstall"
}


resource "aws_api_gateway_method" "Client-getCartInstall-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getCartInstall.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getCartInstall-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getCartInstall.id
  http_method = aws_api_gateway_method.Client-getCartInstall-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getCartInstall-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getCartInstall.id
  http_method = aws_api_gateway_method.Client-getCartInstall-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getCartInstall-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getCartInstall.id
  http_method = aws_api_gateway_method.Client-getCartInstall-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getCartInstall-200.status_code
}


## getClient


resource "aws_api_gateway_resource" "Client-getClient" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getClient"
}


resource "aws_api_gateway_method" "Client-getClient-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getClient.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getClient-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getClient.id
  http_method = aws_api_gateway_method.Client-getClient-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getClient-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getClient.id
  http_method = aws_api_gateway_method.Client-getClient-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getClient-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getClient.id
  http_method = aws_api_gateway_method.Client-getClient-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getClient-200.status_code
}


## getClientPrescriptions


resource "aws_api_gateway_resource" "Client-getClientPrescriptions" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getClientPrescriptions"
}


resource "aws_api_gateway_method" "Client-getClientPrescriptions-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getClientPrescriptions.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getClientPrescriptions-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getClientPrescriptions.id
  http_method = aws_api_gateway_method.Client-getClientPrescriptions-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getClientPrescriptions-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getClientPrescriptions.id
  http_method = aws_api_gateway_method.Client-getClientPrescriptions-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getClientPrescriptions-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getClientPrescriptions.id
  http_method = aws_api_gateway_method.Client-getClientPrescriptions-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getClientPrescriptions-200.status_code
}


## getDefaultPrescriptions


resource "aws_api_gateway_resource" "Client-getDefaultPrescriptions" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getDefaultPrescriptions"
}


resource "aws_api_gateway_method" "Client-getDefaultPrescriptions-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getDefaultPrescriptions.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getDefaultPrescriptions-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getDefaultPrescriptions.id
  http_method = aws_api_gateway_method.Client-getDefaultPrescriptions-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getDefaultPrescriptions-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getDefaultPrescriptions.id
  http_method = aws_api_gateway_method.Client-getDefaultPrescriptions-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getDefaultPrescriptions-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getDefaultPrescriptions.id
  http_method = aws_api_gateway_method.Client-getDefaultPrescriptions-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getDefaultPrescriptions-200.status_code
}


## getQuestions


resource "aws_api_gateway_resource" "Client-getQuestions" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getQuestions"
}


resource "aws_api_gateway_method" "Client-getQuestions-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getQuestions.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getQuestions-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getQuestions.id
  http_method = aws_api_gateway_method.Client-getQuestions-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getQuestions-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getQuestions.id
  http_method = aws_api_gateway_method.Client-getQuestions-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getQuestions-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getQuestions.id
  http_method = aws_api_gateway_method.Client-getQuestions-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getQuestions-200.status_code
}


## getUsage


resource "aws_api_gateway_resource" "Client-getUsage" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "getUsage"
}


resource "aws_api_gateway_method" "Client-getUsage-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-getUsage.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-getUsage-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getUsage.id
  http_method = aws_api_gateway_method.Client-getUsage-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-getUsage-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getUsage.id
  http_method = aws_api_gateway_method.Client-getUsage-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-getUsage-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-getUsage.id
  http_method = aws_api_gateway_method.Client-getUsage-Method.http_method
  status_code = aws_api_gateway_method_response.Client-getUsage-200.status_code
}


## logCartInstall


resource "aws_api_gateway_resource" "Client-logCartInstall" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "logCartInstall"
}


resource "aws_api_gateway_method" "Client-logCartInstall-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-logCartInstall.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-logCartInstall-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-logCartInstall.id
  http_method = aws_api_gateway_method.Client-logCartInstall-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-logCartInstall-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-logCartInstall.id
  http_method = aws_api_gateway_method.Client-logCartInstall-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-logCartInstall-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-logCartInstall.id
  http_method = aws_api_gateway_method.Client-logCartInstall-Method.http_method
  status_code = aws_api_gateway_method_response.Client-logCartInstall-200.status_code
}


## postAnswers


resource "aws_api_gateway_resource" "Client-postAnswers" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "postAnswers"
}


resource "aws_api_gateway_method" "Client-postAnswers-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-postAnswers.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-postAnswers-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-postAnswers.id
  http_method = aws_api_gateway_method.Client-postAnswers-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-postAnswers-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-postAnswers.id
  http_method = aws_api_gateway_method.Client-postAnswers-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-postAnswers-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-postAnswers.id
  http_method = aws_api_gateway_method.Client-postAnswers-Method.http_method
  status_code = aws_api_gateway_method_response.Client-postAnswers-200.status_code
}


## reqFirmwareUpdate


resource "aws_api_gateway_resource" "Client-reqFirmwareUpdate" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "reqFirmwareUpdate"
}


resource "aws_api_gateway_method" "Client-reqFirmwareUpdate-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-reqFirmwareUpdate.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-reqFirmwareUpdate-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-reqFirmwareUpdate.id
  http_method = aws_api_gateway_method.Client-reqFirmwareUpdate-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-reqFirmwareUpdate-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-reqFirmwareUpdate.id
  http_method = aws_api_gateway_method.Client-reqFirmwareUpdate-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-reqFirmwareUpdate-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-reqFirmwareUpdate.id
  http_method = aws_api_gateway_method.Client-reqFirmwareUpdate-Method.http_method
  status_code = aws_api_gateway_method_response.Client-reqFirmwareUpdate-200.status_code
}




## uploadUsage


resource "aws_api_gateway_resource" "Client-uploadUsage" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "uploadUsage"
}


resource "aws_api_gateway_method" "Client-uploadUsage-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-uploadUsage.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-uploadUsage-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-uploadUsage.id
  http_method = aws_api_gateway_method.Client-uploadUsage-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-uploadUsage-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-uploadUsage.id
  http_method = aws_api_gateway_method.Client-uploadUsage-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-uploadUsage-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-uploadUsage.id
  http_method = aws_api_gateway_method.Client-uploadUsage-Method.http_method
  status_code = aws_api_gateway_method_response.Client-uploadUsage-200.status_code
}


## createClient


resource "aws_api_gateway_resource" "Client-createClient" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "createClient"
}


resource "aws_api_gateway_method" "Client-createClient-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-createClient.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-createClient-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-createClient.id
  http_method = aws_api_gateway_method.Client-createClient-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-createClient-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-createClient.id
  http_method = aws_api_gateway_method.Client-createClient-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-createClient-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-createClient.id
  http_method = aws_api_gateway_method.Client-createClient-Method.http_method
  status_code = aws_api_gateway_method_response.Client-createClient-200.status_code
}


## deleteClient


resource "aws_api_gateway_resource" "Client-deleteClient" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Client.id
  path_part   = "deleteClient"
}


resource "aws_api_gateway_method" "Client-deleteClient-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Client-deleteClient.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Client-Authorizer.id
  authorization_scopes  = ["aws.cognito.signin.user.admin"]
}


resource "aws_api_gateway_integration" "Client-deleteClient-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-deleteClient.id
  http_method = aws_api_gateway_method.Client-deleteClient-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Client-deleteClient-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-deleteClient.id
  http_method = aws_api_gateway_method.Client-deleteClient-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Client-deleteClient-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id = aws_api_gateway_resource.Client-deleteClient.id
  http_method = aws_api_gateway_method.Client-deleteClient-Method.http_method
  status_code = aws_api_gateway_method_response.Client-deleteClient-200.status_code
}


## Therapist-Authorizers


resource "aws_api_gateway_authorizer" "Therapist-Authorizer" {
  name          = "Therapist-Authorizer"
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  type          = "COGNITO_USER_POOLS"
  provider_arns = [var.Therapist-Authorizer]
}


## Therapist-Resource


resource "aws_api_gateway_resource" "Therapist" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_rest_api.MyDemoAPI.root_resource_id
  path_part   = "Therapist"
}


## CutitronicsLookUp


resource "aws_api_gateway_resource" "Therapist-CutitronicsLookUp" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "CutitronicsLookUp"
}


resource "aws_api_gateway_method" "Therapist-CutitronicsLookUp-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-CutitronicsLookUp.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-CutitronicsLookUp-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-CutitronicsLookUp.id
  http_method = aws_api_gateway_method.Therapist-CutitronicsLookUp-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-CutitronicsLookUp-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-CutitronicsLookUp.id
  http_method = aws_api_gateway_method.Therapist-CutitronicsLookUp-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-CutitronicsLookUp-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-CutitronicsLookUp.id
  http_method = aws_api_gateway_method.Therapist-CutitronicsLookUp-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-CutitronicsLookUp-200.status_code
}


## getGuest


resource "aws_api_gateway_resource" "Therapist-getGuest" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getGuest"
}


resource "aws_api_gateway_method" "Therapist-getGuest-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getGuest.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getGuest-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getGuest.id
  http_method = aws_api_gateway_method.Therapist-getGuest-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getGuest-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getGuest.id
  http_method = aws_api_gateway_method.Therapist-getGuest-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getGuest-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getGuest.id
  http_method = aws_api_gateway_method.Therapist-getGuest-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getGuest-200.status_code
}


## getAllGuests


resource "aws_api_gateway_resource" "Therapist-getAllGuests" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getAllGuests"
}


resource "aws_api_gateway_method" "Therapist-getAllGuests-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllGuests.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getAllGuests-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllGuests.id
  http_method = aws_api_gateway_method.Therapist-getAllGuests-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getAllGuests-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllGuests.id
  http_method = aws_api_gateway_method.Therapist-getAllGuests-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getAllGuests-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllGuests.id
  http_method = aws_api_gateway_method.Therapist-getAllGuests-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getAllGuests-200.status_code
}


## postGuestNotes


resource "aws_api_gateway_resource" "Therapist-postGuestNotes" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "postGuestNotes"
}

resource "aws_api_gateway_method" "Therapist-postGuestNotes-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-postGuestNotes.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-postGuestNotes-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-postGuestNotes.id
  http_method = aws_api_gateway_method.Therapist-postGuestNotes-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-postGuestNotes-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-postGuestNotes.id
  http_method = aws_api_gateway_method.Therapist-postGuestNotes-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-postGuestNotes-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-postGuestNotes.id
  http_method = aws_api_gateway_method.Therapist-postGuestNotes-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-postGuestNotes-200.status_code
}


## createSession


resource "aws_api_gateway_resource" "Therapist-createSession" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "createSession"
}


resource "aws_api_gateway_method" "Therapist-createSession-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-createSession.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-createSession-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-createSession.id
  http_method = aws_api_gateway_method.Therapist-createSession-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Therapist-createSession-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-createSession.id
  http_method = aws_api_gateway_method.Therapist-createSession-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-createSession-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-createSession.id
  http_method = aws_api_gateway_method.Therapist-createSession-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-createSession-200.status_code
}


## updateSession


resource "aws_api_gateway_resource" "Therapist-updateSession" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "updateSession"
}


resource "aws_api_gateway_method" "Therapist-updateSession-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-updateSession.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-updateSession-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-updateSession.id
  http_method = aws_api_gateway_method.Therapist-updateSession-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }    
}


resource "aws_api_gateway_method_response" "Therapist-updateSession-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-updateSession.id
  http_method = aws_api_gateway_method.Therapist-updateSession-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-updateSession-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-updateSession.id
  http_method = aws_api_gateway_method.Therapist-updateSession-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-updateSession-200.status_code
}


## getClientPrescriptions


resource "aws_api_gateway_resource" "Therapist-getClientPrescriptions" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getClientPrescriptions"
}


resource "aws_api_gateway_method" "Therapist-getClientPrescriptions-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getClientPrescriptions.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getClientPrescriptions-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getClientPrescriptions.id
  http_method = aws_api_gateway_method.Therapist-getClientPrescriptions-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }    
}


resource "aws_api_gateway_method_response" "Therapist-getClientPrescriptions-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getClientPrescriptions.id
  http_method = aws_api_gateway_method.Therapist-getClientPrescriptions-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getClientPrescriptions-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getClientPrescriptions.id
  http_method = aws_api_gateway_method.Therapist-getClientPrescriptions-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getClientPrescriptions-200.status_code
}


## getUsage


resource "aws_api_gateway_resource" "Therapist-getUsage" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getUsage"
}


resource "aws_api_gateway_method" "Therapist-getUsage-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getUsage.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getUsage-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getUsage.id
  http_method = aws_api_gateway_method.Therapist-getUsage-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getUsage-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getUsage.id
  http_method = aws_api_gateway_method.Therapist-getUsage-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getUsage-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getUsage.id
  http_method = aws_api_gateway_method.Therapist-getUsage-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getUsage-200.status_code
}



## getBrandProducts


resource "aws_api_gateway_resource" "Therapist-getBrandProducts" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getBrandProducts"
}


resource "aws_api_gateway_method" "Therapist-getBrandProducts-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getBrandProducts.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getBrandProducts-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getBrandProducts.id
  http_method = aws_api_gateway_method.Therapist-getBrandProducts-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getBrandProducts-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getBrandProducts.id
  http_method = aws_api_gateway_method.Therapist-getBrandProducts-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getBrandProducts-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getBrandProducts.id
  http_method = aws_api_gateway_method.Therapist-getBrandProducts-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getBrandProducts-200.status_code
}


## getDefaultPrescriptions


resource "aws_api_gateway_resource" "Therapist-getDefaultPrescriptions" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getDefaultPrescriptions"
}


resource "aws_api_gateway_method" "Therapist-getDefaultPrescriptions-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getDefaultPrescriptions.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getDefaultPrescriptions-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getDefaultPrescriptions.id
  http_method = aws_api_gateway_method.Therapist-getDefaultPrescriptions-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getDefaultPrescriptions-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getDefaultPrescriptions.id
  http_method = aws_api_gateway_method.Therapist-getDefaultPrescriptions-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getDefaultPrescriptions-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getDefaultPrescriptions.id
  http_method = aws_api_gateway_method.Therapist-getDefaultPrescriptions-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getDefaultPrescriptions-200.status_code
}


## getSession


resource "aws_api_gateway_resource" "Therapist-getSession" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getSession"
}


resource "aws_api_gateway_method" "Therapist-getSession-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getSession.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getSession-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getSession.id
  http_method = aws_api_gateway_method.Therapist-getSession-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getSession-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getSession.id
  http_method = aws_api_gateway_method.Therapist-getSession-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getSession-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getSession.id
  http_method = aws_api_gateway_method.Therapist-getSession-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getSession-200.status_code
}


## getAllClients


resource "aws_api_gateway_resource" "Therapist-getAllClients" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getAllClients"
}


resource "aws_api_gateway_method" "Therapist-getAllClients-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllClients.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getAllClients-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllClients.id
  http_method = aws_api_gateway_method.Therapist-getAllClients-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }
}


resource "aws_api_gateway_method_response" "Therapist-getAllClients-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllClients.id
  http_method = aws_api_gateway_method.Therapist-getAllClients-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getAllClients-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllClients.id
  http_method = aws_api_gateway_method.Therapist-getAllClients-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getAllClients-200.status_code
}


## getAllTherapists


resource "aws_api_gateway_resource" "Therapist-getAllTherapists" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_resource.Therapist.id
  path_part   = "getAllTherapists"
}


resource "aws_api_gateway_method" "Therapist-getAllTherapists-Method" {
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllTherapists.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.Therapist-Authorizer.id
}


resource "aws_api_gateway_integration" "Therapist-getAllTherapists-Integration" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllTherapists.id
  http_method = aws_api_gateway_method.Therapist-getAllTherapists-Method.http_method
  type        = "MOCK"
  request_templates = {"application/json" = "{ statusCode: 200 }"  }  
}


resource "aws_api_gateway_method_response" "Therapist-getAllTherapists-200" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllTherapists.id
  http_method = aws_api_gateway_method.Therapist-getAllTherapists-Method.http_method
  status_code = "200"
}


resource "aws_api_gateway_integration_response" "Therapist-getAllTherapists-Response" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  resource_id   = aws_api_gateway_resource.Therapist-getAllTherapists.id
  http_method = aws_api_gateway_method.Therapist-getAllTherapists-Method.http_method
  status_code = aws_api_gateway_method_response.Therapist-getAllTherapists-200.status_code
}



## Admin-Resource


resource "aws_api_gateway_resource" "Admin" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_rest_api.MyDemoAPI.root_resource_id
  path_part   = "Admin"
}


## Admin-Authorizers


resource "aws_api_gateway_authorizer" "Admin-Authorizer" {
  name          = "Admin-Authorizer"
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  type          = "COGNITO_USER_POOLS"
  provider_arns = [var.Therapist-Authorizer]
}


## System-Resource


resource "aws_api_gateway_resource" "System" {
  rest_api_id = aws_api_gateway_rest_api.MyDemoAPI.id
  parent_id   = aws_api_gateway_rest_api.MyDemoAPI.root_resource_id
  path_part   = "System"
}


resource "aws_api_gateway_authorizer" "System-Authorizer" {
  name          = "System-Authorizer"
  rest_api_id   = aws_api_gateway_rest_api.MyDemoAPI.id
  type          = "COGNITO_USER_POOLS"
  provider_arns = [var.System-Authorizer]
}