variable "Client-Authorizer" {
  type        = string
  description = "Client-Authorizer ARN"
}


variable "Therapist-Authorizer" {
  type        = string
  description = "Therapist-Authorizer ARN"
}


variable "Admin-Authorizer" {
  type        = string
  description = "Admin-Authorizer ARN"
}


variable "System-Authorizer" {
  type        = string
  description = "System-Authorizer ARN"
}